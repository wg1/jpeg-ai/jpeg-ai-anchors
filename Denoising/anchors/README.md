
# OVERVIEW

This source code provides a PyTorch implementation of FFDNet image denoising,
as in Zhang, Kai, Wangmeng Zuo, and Lei Zhang. "FFDNet: Toward a 
fast and flexible solution for CNN based image denoising".

The original code was edited to generate the anchor results for the JPEG AI Compressed Domain Denoising task. 

# USER GUIDE

## Environment Setup

1) `cd Denoising/anchors/`

2) Set up the conda environment through the ffdnet-pytorch.yml file: `conda env create -f ffdnet-pytorch.yml`


More information on the creation of the environment from a yml file are available at https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#creating-an-environment-from-an-environment-yml-file

3) Activate the environment: `conda activate ffdnet-pytorch`

## Usage

### 1. Original Anchor

This section includes the information to generate the results for the original anchor. The script will denoise all the files in the noisy testset and return the information about the computational complexity.

1) Copy the JPEG testset in folder `./data/testset/`. The images in this folder are expected to follow the JPEG AI naming convention. As an example: 00001_TE_2096x1400.png

2) Copy the provided noisy images in folder `./data/noisy_testset/`. The images in this folder are expected to follow the JPEG AI naming convention. As an example: 00001_TE_2096x1400_n1.png

3) Execute the script by:
```
python test_ffdnet_jpegai.py --anchor original
```
The script will automatically load all the noisy images in folder data/noisy_tests and the corresponding images in data/testset. The sigma value of the noise is automatically estimated. The script returns the file "flopscount_ffdnet_original.csv" with information about the complexity of the model.




### 2. Decoded Anchor

This section includes the information to generate the results for the decoded anchor. The script will denoise all the files in the noisy testset decoded and return the information about the computational complexity.

1) Copy the noisy decoded images in folder `./data/noisy_decoded/`. Please note that all the images should be copied inside this folder, without subfolders. The images in this folder are expected to follow the JPEG AI naming convention. As an example: CODEC_00001_TE_2096x1400_n1_8bit_sRGB_012.png

2) Execute the script by:
```
python test_ffdnet_jpegai.py --anchor decoded
```
The script will automatically load all the noisy images in folder data/noisy_decoded and the corresponding images in data/testset. The sigma value of the noise is automatically estimated. The script returns the file "flopscount_ffdnet_decoded.csv" with information about the complexity of the model.

### 3. Compute objective metrics

If you haven't set up the metrics environment, run the following:

1) Go to the metrics directory: ``cd ../../metrics/``
2) Create the conda environment: ``conda create -n jpeg_ai_metrics python=3.6.7``
3) Activate the environment: ``conda activate jpeg_ai_metrics``
4) Upgrade `pip` by a command `python -m pip install --upgrade pip`
5) Install dependencies: ``pip install -r requirements.txt``
6) Download [vmaf.linux](https://github.com/Netflix/vmaf/releases/download/v2.1.1/vmaf.linux) version 2.1.1 to the root directory of the project by command:
``wget https://github.com/Netflix/vmaf/releases/download/v2.1.1/vmaf.linux``
7) Make it executable: `chmod +x vmaf.linux`
8) Go back to Denoising directory:`cd ../Denoising/anchors/`

If your environment for the computation of the objective metrics is already set up, you just need to ``conda activate jpeg_ai_metrics``

9) Run the following:

```
cp compute_metrics_denoising.py ../../metrics/
cd ../../metrics/
python compute_metrics_denoising.py --csv
```

10) Move the files in the denoising folder: `cp *.csv ../Denoising/anchors/data/`



# ABOUT THE ORIGINAL CODE

The original code can be downloaded from http://www.ipol.im/pub/art/2019/231/

* Author    : Matias Tassano <matias.tassano@parisdescartes.fr>
* Copyright : (C) 2018 IPOL Image Processing On Line http://www.ipol.im/
* Licence   : GPL v3+, see GPLv3.txt

* Edited by Michela Testolina <michela.testolina@epfl.ch>


# COPYRIGHT

Copyright notice of the original code: 

Copyright 2018 IPOL Image Processing On Line http://www.ipol.im/

Copying and distribution of this file, with or without modification,
are permitted in any medium without royalty provided the copyright
notice and this notice are preserved.  This file is offered as-is,
without any warranty.

