"""
Denoise an image with the FFDNet denoising method

Copyright (C) 2018, Matias Tassano <matias.tassano@parisdescartes.fr>

This program is free software: you can use, modify and/or
redistribute it under the terms of the GNU General Public
License as published by the Free Software Foundation, either
version 3 of the License, or (at your option) any later
version. You should have received a copy of this license along
this program. If not, see <http://www.gnu.org/licenses/>.
"""

import os
import glob
import argparse
import time
import numpy as np
import cv2
import torch
import torch.nn as nn
from torch.autograd import Variable
from models import FFDNet
from utils import batch_psnr, normalize,\
				variable_to_cv2_image, remove_dataparallel_wrapper, is_rgb
from skimage import img_as_float
from skimage.io import imread, imsave
from skimage.restoration import denoise_wavelet
from ptflops.flops_counter import  flops_to_string
import csv

os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = "0"


def test_ffdnet(**args):
	r"""Denoises an input image with FFDNet
	"""
	# Init logger

	original_folder = args['original_folder']
	

	# Check if input exists and if it is RGB
	try:
		rgb_den = is_rgb(args['input'])
	except:
		raise Exception('Could not open the input image')

	# Open image as a CxHxW torch.Tensor
	if rgb_den:
		in_ch = 3
		model_fn = args['model'] #'models/net_rgb.pth'
		imnoisy = cv2.imread(args['input'])
		imnoisy = imnoisy.astype('uint8')
		# from HxWxC to CxHxW, RGB image
		imnoisy = (cv2.cvtColor(imnoisy, cv2.COLOR_BGR2RGB)).transpose(2, 0, 1)

	imnoisy = np.expand_dims(imnoisy, 0)

	# Handle odd sizes
	expanded_h = False
	expanded_w = False
	sh_im = imnoisy.shape
	if sh_im[2]%2 == 1:
		expanded_h = True
		imnoisy = np.concatenate((imnoisy, \
				imnoisy[:, :, -1, :][:, :, np.newaxis, :]), axis=2)

	if sh_im[3]%2 == 1:
		expanded_w = True
		imnoisy = np.concatenate((imnoisy, \
				imnoisy[:, :, :, -1][:, :, :, np.newaxis]), axis=3)

	imnoisy = normalize(imnoisy)
	imnoisy = torch.Tensor(imnoisy)

	# Absolute path to model file
	model_fn = os.path.join(os.path.abspath(os.path.dirname(__file__)), \
				model_fn)

	# Create model
	#print('Loading model ...\n')
	net = FFDNet(num_input_channels=in_ch)

	# Load saved weights
	if args['cuda']:
		state_dict = torch.load(model_fn)
		device_ids = [0]
		model = nn.DataParallel(net, device_ids=device_ids).cuda()
	else:
		state_dict = torch.load(model_fn, map_location='cpu')
		# CPU mode: remove the DataParallel wrapper
		state_dict = remove_dataparallel_wrapper(state_dict)
		model = net
	model.load_state_dict(state_dict)

	# Sets the model in evaluation mode
	model.eval()

	# Sets data type according to CPU or GPU modes
	if args['cuda']:
		dtype = torch.cuda.FloatTensor
	else:
		dtype = torch.FloatTensor


	# Get original image
	if args['anchor'] == "original":
		original_filename = args['input'][19:24]+args['input'][28:]
	elif args['anchor'] == "decoded":
		original_filename = '_'.join(args['input'].split("_")[2:])
		original_filename = original_filename[:6]+original_filename[10:-8]+'.png'
	imorig = cv2.imread(os.path.join(original_folder, original_filename))
	imorig = imorig.astype('uint8')
	imorig = (cv2.cvtColor(imorig, cv2.COLOR_BGR2RGB)).transpose(2, 0, 1)
	imorig = np.expand_dims(imorig, 0)

	# Handle odd sizes
	expanded_h = False
	expanded_w = False
	sh_im = imorig.shape
	if sh_im[2]%2 == 1:
		expanded_h = True
		imorig = np.concatenate((imorig, \
				imorig[:, :, -1, :][:, :, np.newaxis, :]), axis=2)

	if sh_im[3]%2 == 1:
		expanded_w = True
		imorig = np.concatenate((imorig, \
				imorig[:, :, :, -1][:, :, :, np.newaxis]), axis=3)

	imorig = normalize(imorig)
	imorig = torch.Tensor(imorig)




    # DENOISE WITH FFDNET
	with torch.no_grad(): # PyTorch v0.4.0
		noise = imnoisy-imorig
		noise_sigma = torch.std(noise).item()
		#print(noise_sigma)
		imorig, imnoisy = Variable(imorig.type(dtype)), \
	    				Variable(imnoisy.type(dtype))


		nsigma = Variable(
	    		torch.FloatTensor([noise_sigma]).type(dtype))

	
	model = init_ptflops_calc(model) # for complexity analysis

	# Estimate noise and subtract it to the input image
	im_noise_estim = model(imnoisy, nsigma)
	outim = torch.clamp(imnoisy-im_noise_estim, 0., 1.)

	# Complexity analysis 
	flops_count = finish_ptflops_calc(model)
	h,w = imnoisy.shape[-2:]
	print("==================")
	print("Flops: {0}, i.e {1} / pxl".format(flops_to_string(flops_count, units=None), flops_to_string(flops_count / (h*w), units='Mac')))
	print("==================")


	if expanded_h:
		imorig = imorig[:, :, :-1, :]
		outim = outim[:, :, :-1, :]
		imnoisy = imnoisy[:, :, :-1, :]

	if expanded_w:
		imorig = imorig[:, :, :, :-1]
		outim = outim[:, :, :, :-1]
		imnoisy = imnoisy[:, :, :, :-1]

	# Compute difference
	diffout   = 2*(outim - imorig) + .5
	diffnoise = 2*(imnoisy-imorig) + .5


	# Save image
	#noisyimg = variable_to_cv2_image(imnoisy)
	#origimg = variable_to_cv2_image(imorig)
	outimg = variable_to_cv2_image(outim)

	if args['anchor'] == "original":
		noisy_number = args['input'].split("_")[2]
		#out_name = 'FFDNet-orig-'+noisy_number+'_'+args['input'][19:24]+args['input'][28:-4]+'_8bit_sRGB_000.png'
		out_name = 'FFDNet-orig-'+args['input'][19:-4]+'_000.png'
		out_dir = os.path.join(args['out_dir_ffdnet'], args['out_dir_ffdnet'].split("/")[-1]+"-"+noisy_number)
	elif args['anchor'] == "decoded":
		noisy_number = args['input'].split("_")[3]
		out_name = 'FFDNet-dec-'+args['input'][19:]
		out_dir = os.path.join(args['out_dir_ffdnet'], args['out_dir_ffdnet'].split("/")[-1]+"-"+noisy_number)

	
	os.makedirs(out_dir, exist_ok=True)


	cv2.imwrite(os.path.join(out_dir,out_name), outimg)

	return flops_to_string(flops_count, units=None), flops_to_string(flops_count / (h*w), units='Mac')




def test_wavelet(**args):
	r"""Denoises an input image with FFDNet
	"""
	# DENOISE WITH WAVELET THRESHOLDING
	original_folder = args['original_folder']


	noisy = img_as_float(imread(args['input']))

	if args['anchor'] == "original":
		original_filename = args['input'][19:24]+args['input'][28:]#'_'.join(args['input'].split("/")[-1].split("_")[:-1])
	elif args['anchor'] == "decoded":
		#original_filename = '_'.join(args['input'].split("/")[-1].split("_")[1:-4])
		original_filename = '_'.join(args['input'].split("_")[2:])
		original_filename = original_filename[:6]+original_filename[10:-8]+'.png'
	
	orig = img_as_float(imread(os.path.join(original_folder, original_filename)))
	noise_map = noisy-orig
	sigma_est = np.std(noise_map)

	im_bayes = denoise_wavelet(noisy, multichannel=True, convert2ycbcr=True, method='BayesShrink', mode='soft', sigma=sigma_est)
	im_bayes = np.clip(im_bayes, a_min = 0, a_max = 1)

	if args['anchor'] == "original":
		noisy_number = args['input'].split("_")[2]
		out_name = 'Wavelet-orig-'+args['input'][19:-4]+'_000.png'
		out_dir = os.path.join(args['out_dir_wavelet'], args['out_dir_wavelet'].split("/")[-1]+"-"+noisy_number)
	elif args['anchor'] == "decoded":
		noisy_number = args['input'].split("_")[3]
		out_name = 'Wavelet-dec-'+args['input'][19:]
		out_dir = os.path.join(args['out_dir_wavelet'], args['out_dir_wavelet'].split("/")[-1]+"-"+noisy_number)
	
	os.makedirs(out_dir, exist_ok=True)


	imsave(os.path.join(out_dir,out_name), (im_bayes*255.).astype(np.uint8))



def init_ptflops_calc(model):
    import sys
    from ptflops.flops_counter import add_flops_counting_methods  
    ans = add_flops_counting_methods(model)
    ans.start_flops_count(ost=sys.stdout, verbose=False,
                                ignore_list=[])
    ans.reset_flops_count()
    return ans

def finish_ptflops_calc(model):
    ## Flops calculation
    flops_count, _ = model.compute_average_flops_cost()
    model.reset_flops_count()
    return flops_count



if __name__ == "__main__":
	# Parse arguments
	parser = argparse.ArgumentParser(description="FFDNet_Test")
	parser.add_argument("--anchor", type=str, default="original", \
						help='the type of anchor to process. Choose between original and decoded')
	parser.add_argument("--input", type=str, default="", \
						help='path to input image')
	parser.add_argument("--noisy_folder", type=str, default="data/noisy_testset", \
						help='directory to the noisy testset')
	parser.add_argument("--original_folder", type=str, default="data/testset", \
						help='directory to the original testset')
	parser.add_argument("--model", type=str, default="net_rgb.pth", \
						help='model used for denoising')
	parser.add_argument("--out_dir_ffdnet", type=str, default="data/output/FFDNet-orig", \
						help='directory where to save the denoised images')
	parser.add_argument("--out_dir_wavelet", type=str, default="data/output/Wavelet-orig", \
						help='directory where to save the denoised images')
	parser.add_argument("--no_gpu", action='store_true', \
						help="run model on CPU")
	argspar = parser.parse_args()


	# String to bool
	#argspar.add_noise = (argspar.add_noise.lower() == 'true')

	# use CUDA?
	argspar.cuda = not argspar.no_gpu and torch.cuda.is_available()
	use_cuda = argspar.cuda

	if argspar.anchor == "original":
		argspar.noisy_folder = 'data/noisy_testset'
		argspar.out_dir_ffdnet = "data/output/FFDNet-orig"
		argspar.out_dir_wavelet = "data/output/Wavelet-orig"
	elif argspar.anchor == "decoded":
		argspar.noisy_folder = 'data/noisy_decoded'
		argspar.out_dir_ffdnet = "data/output/FFDNet-dec"
		argspar.out_dir_wavelet = "data/output/Wavelet-dec"



	print("\n### Testing FFDNet model ###")
	#print("> Parameters:")
	#for p, v in zip(argspar.__dict__.keys(), argspar.__dict__.values()):
	#	print('\t{}: {}'.format(p, v))
	#print('\n')

	flops_count_list = []
	flops_pix_count_list = []
	img_list = []

	for img in glob.glob(argspar.noisy_folder+"/*.png"):
		print("Processing image " + img)
		argspar.input = img
		try:
			argspar.cuda = use_cuda
			flops_count, flops_pix_count = test_ffdnet(**vars(argspar))
			flops_count_list.append(flops_count)
			flops_pix_count_list.append(flops_pix_count)
			img_list.append(img.split("/")[-1])
			
		except Exception as e:
			print(e)
			#print("Skipping this image.")
			print("Trying with CPU...")

			try:
				argspar.cuda = False
				flops_count,flops_pix_count = test_ffdnet(**vars(argspar))
				flops_count_list.append(flops_count)
				flops_pix_count_list.append(flops_pix_count)
				img_list.append(img.split("/")[-1])

			except Exception as e:
				print(e)
				print("Failed. Skipping this image.")
				pass

	
	with open('flopscount_ffdnet_'+str(argspar.anchor)+'.csv', 'w') as f:
		writer = csv.writer(f)
		writer.writerow(['IMAGE','FLOPS (GMac)', 'FLOPS (Mac / pxl)'])
		writer.writerows(zip(img_list, flops_count_list, flops_pix_count_list))

	print('\nDone. Images saved in folder '+argspar.out_dir_ffdnet)



	
	print('\n\n### Testing Wavelet thresholding denoising ###')
	for img in glob.glob(argspar.noisy_folder+"/*.png"):
		print("Processing image " + img)
		argspar.input = img
		test_wavelet(**vars(argspar))
	print('\nDone. Images saved in folder '+argspar.out_dir_wavelet)
	
