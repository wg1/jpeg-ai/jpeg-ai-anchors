import torch
import torch.nn as nn
import numpy as np
import sys
import os
sys.path.insert(0, os.path.join(os.getcwd(), os.path.pardir))
from metrics import MetricsProcessor
import argparse


if __name__ == "__main__":

    # Initialize object for metrics calculation
    proc = MetricsProcessor()


    ap = argparse.ArgumentParser()
    # Add command-line arguments from metrics calculation class (if it is needed)
    ap.add_argument('--csv', default=False, action="store_true", help="Output file in CSV format")
    proc.add_arguments(ap)
    # Parse teh arguments
    proc.parse_arguments(ap)
    args = ap.parse_args()

    for folder in os.listdir("../Denoising/anchors/data/output/"):

        summary_name = "summary_"+folder+"_denoised_"

        for noise in range(1,4):
            print("Computing "+summary_name+str(noise).zfill(3)+".csv ..." )
            summary_fn = summary_name+str(noise).zfill(3)+".csv" if args.csv else summary_name+str(noise).zfill(3)+".txt"
            ## Initialization of the output file
            proc.init_summary_file(summary_fn, "csv" if args.csv else "txt")

            for file in os.listdir(os.path.join("../Denoising/anchors/data/output",folder,folder+"-"+str(noise).zfill(3))):
                ## get denoised and original image
                den = os.path.join("../Denoising/anchors/data/output",folder,folder+"-"+str(noise).zfill(3),file)
                if len(file.split("-")[2].split("_"))==7:
                    ori_name = '_'.join(file.split("-")[2].split("_")[2:6])+".png"
                    ori_name = file.split("-")[2].split("_")[0] +'_'+ ori_name
                elif len(file.split("-")[2].split("_"))==8:
                    ori_name = '_'.join(file.split("-")[2].split("_")[3:7])+".png"
                    ori_name = file.split("-")[2].split("_")[1] +'_'+ ori_name
                ori = os.path.join("../Denoising/anchors/data/testset", ori_name)

                
                ## Metrics calculation based on original and reconstructed file
                metrics = proc.process_image_files(ori, den)
                ## Write all metrics to the output file. `prefix_data` is only needed in CSV mode
                
                if folder.split("-")[-1] == "orig":
                    proc.write_data(file, 0, metrics, prefix_data=['_'.join(file.split("_")[1:-4])+".png", "ORIG_ANCHOR"])
                elif folder.split("-")[-1] == "dec":
                    proc.write_data(file, den.split("_")[-1].split(".")[0], metrics, prefix_data=['_'.join(file.split("_")[1:-4])+".png", "DECODED_ANCHOR"])
            
            ## Close the output file
            proc.close_file()
    
    

    


