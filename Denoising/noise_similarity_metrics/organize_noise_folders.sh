main_dir="."
bits_extension="bits"
evaluation_code_dir="somedir/noise_similarity_metrics/"
original_testset="somedir/testset/"
noisy_testset="somedir/noisy_testset/"
base_folder="somedir/Submissions/"
clean_dec="somedir/Clean_dec/"
codec=BMSHJ
N_realization=3
for (( i=1; i<$N_realization+1; i++ ));
do
  dirname=${codec}
  if [ -d "$dirname" ]
  then
    rm -r $dirname
  fi  
  mkdir ./$dirname
  mkdir ./$dirname/bit
  mkdir ./$dirname/rec
  cp -r $main_dir/bit/*_00${i}_* ./$dirname/bit/
  cp -r $main_dir/rec/*_00${i}_* ./$dirname/rec/
  python ${evaluation_code_dir}main_noise_report.py $original_testset $base_folder $dirname  $noisy_testset $clean_dec --n_noise $i --rates 12 25 50   --bin-ext $bits_extension --csv
  dirname_after=${codec}_n${i}
  if [ -d "$dirname_after" ]
  then
    rm -r $dirname_after
  fi  
  mv ./$dirname ./$dirname_after
  rm -r ./$dirname_after
  mv ${codec}_summary.txt ${codec}_summary_n${i}.txt
  mv ${codec}_summary.csv ${codec}_summary_n${i}.csv
  echo $dirname_after
done
