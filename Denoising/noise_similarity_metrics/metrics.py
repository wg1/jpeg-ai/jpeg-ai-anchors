import torch
import numpy as np
import os

class DataClass:

    def __init__(self):
        self.data_range = [0,1]
        self.yuv_data = {}
        self.rgb_data = None
        self.shape = []          # height, width
        self.bitdepth = -1

    def load_image(self, filename, def_bits = 10, def_fmt="444", device="cpu", color_conv="709"):
        ext = filename[-4:].lower()
        bitdepth_ans = def_fmt
        if ext == ".yuv":
            w,h,b,fmt = DataClass.extract_info(filename, def_bits, def_fmt)
            if def_bits == -1:
                def_bits = b
            assert def_bits > 0
            self.shape = [h,w]
            yuv_data = DataClass.read_yuv(filename, w, h, b, fmt=fmt, device=device, out_plane_norm=self.data_range) 

            for plane in yuv_data:
                self.yuv_data[plane] = DataClass.round_plane(yuv_data[plane], def_bits)
            
            yuv_t = self.convert_yuvdict_to_tensor(self.yuv_data)
            self.rgb_data = DataClass.yuv_to_rgb(yuv_t, color_conv)
            # Should we round RGB to n bits? 
            self.rgb_data = DataClass.round_plane(self.rgb_data, def_bits)
            bitdepth_ans = def_bits
        elif ext == ".png" or ext == ".jpg":
            from PIL import Image
            with Image.open(filename) as im:
                mode = im.mode
                rgb_data = np.array(im.convert("RGB"))
            if def_bits == -1:
                if ";" in mode:
                    # TODO: Check support of this feature
                    s_tmp = mode.split(";")
                    def_bits = int(s_tmp[1])
                else:
                    def_bits = 8
            self.rgb_data = torch.tensor(rgb_data, dtype=torch.float, device=device).permute(2,0,1)
            self.rgb_data = DataClass.convert_and_round_plane(self.rgb_data, [0, 255], self.data_range, def_bits).unsqueeze(0)
            yuv_t = self.rgb_to_yuv(self.rgb_data, color_conv).clamp(min(self.data_range), max(self.data_range))
            yuv_t = DataClass.round_plane(yuv_t, def_bits)
            self.shape = yuv_t.shape[-2:]
            bitdepth_ans = def_bits
            self.yuv_data = {
                'Y': yuv_t[0,0],
                'U': yuv_t[0,1],
                'V': yuv_t[0,2]
            }
        else:
            raise NotImplementedError
        self.bitdepth = bitdepth_ans
        return self, bitdepth_ans


    @staticmethod
    def round_plane(plane, bits):
        return plane.mul((1 << bits)-1).round().div((1 << bits)-1)

    @staticmethod
    def convertup_and_round_plane(plane, cur_range, new_range, bits):
        return DataClass.convert_range(plane, cur_range, new_range).mul((1 << bits)-1).round()

    @staticmethod
    def convert_and_round_plane(plane, cur_range, new_range, bits):
        return DataClass.round_plane(DataClass.convert_range(plane, cur_range, new_range),bits) 

    @staticmethod
    def convert_range(plane, cur_range, new_range=[0,1]):
        if cur_range[0] == new_range[0] and cur_range[1] == new_range[1]:
            return plane
        return (plane + cur_range[0]) * (new_range[1] - new_range[0]) / (cur_range[1] - cur_range[0]) - new_range[0]


    @staticmethod
    def convert_yuvdict_to_tensor(yuv, device="cpu"):
        size = yuv['Y'].shape
        c = len(yuv)
        ans = torch.zeros( (1, c, size[-2], size[-1]), dtype=torch.float, device=torch.device(device))
        ans[:,0,:,:] = yuv['Y']
        ans[:,1,:,:] = yuv['U'] if 'U' in yuv else yuv['Y']
        ans[:,2,:,:] = yuv['V'] if 'V' in yuv else yuv['Y']
        return ans

    @staticmethod
    def color_conv_matrix(color_conv = "709"):
        if color_conv == "601":
            # BT.601
            a = 0.299
            b = 0.587
            c = 0.114
            d = 1.772
            e = 1.402
        elif color_conv == "709":
            # BT.709
            a = 0.2126
            b = 0.7152
            c = 0.0722
            d = 1.8556
            e = 1.5748
        elif color_conv == "2020":
            # BT.2020
            a = 0.2627
            b = 0.6780
            c = 0.0593
            d = 1.8814
            e = 1.4747
        else:
            raise NotImplementedError

        return a,b,c,d,e

    @staticmethod
    def yuv_to_rgb(image: torch.Tensor, color_conv="709") -> torch.Tensor:
        r"""Convert an YUV image to RGB.

        The image data is assumed to be in the range of (0, 1).

        Args:
            image (torch.Tensor): YUV Image to be converted to RGB with shape :math:`(*, 3, H, W)`.

        Returns:
            torch.Tensor: RGB version of the image with shape :math:`(*, 3, H, W)`.

        Example:
            >>> input = torch.rand(2, 3, 4, 5)
            >>> output = yuv_to_rgb(input)  # 2x3x4x5

        Took from https://kornia.readthedocs.io/en/latest/_modules/kornia/color/yuv.html#rgb_to_yuv
        """
        if not isinstance(image, torch.Tensor):
            raise TypeError("Input type is not a torch.Tensor. Got {}".format(
                type(image)))

        if len(image.shape) < 3 or image.shape[-3] != 3:
            raise ValueError("Input size must have a shape of (*, 3, H, W). Got {}"
                            .format(image.shape))

        y: torch.Tensor = image[..., 0, :, :]
        u: torch.Tensor = image[..., 1, :, :] - 0.5
        v: torch.Tensor = image[..., 2, :, :] - 0.5

        #r: torch.Tensor = y + 1.14 * v  # coefficient for g is 0
        #g: torch.Tensor = y + -0.396 * u - 0.581 * v
        #b: torch.Tensor = y + 2.029 * u  # coefficient for b is 0

        a,b,c,d,e = DataClass.color_conv_matrix(color_conv)
    
        
        r: torch.Tensor = y + e * v  # coefficient for g is 0
        g: torch.Tensor = y - (c * d / b) * u - (a * e / b) * v
        b: torch.Tensor = y + d * u  # coefficient for b is 0

        out: torch.Tensor = torch.stack([r, g, b], -3)

        return out


    @staticmethod
    def rgb_to_yuv(image: torch.Tensor, color_conv="709") -> torch.Tensor:
        r"""Convert an RGB image to YUV.

        The image data is assumed to be in the range of (0, 1).

        Args:
            image (torch.Tensor): RGB Image to be converted to YUV with shape :math:`(*, 3, H, W)`.

        Returns:
            torch.Tensor: YUV version of the image with shape :math:`(*, 3, H, W)`.

        Example:
            >>> input = torch.rand(2, 3, 4, 5)
            >>> output = rgb_to_yuv(input)  # 2x3x4x5
        """
        if not isinstance(image, torch.Tensor):
            raise TypeError("Input type is not a torch.Tensor. Got {}".format(type(image)))

        if len(image.shape) < 3 or image.shape[-3] != 3:
            raise ValueError("Input size must have a shape of (*, 3, H, W). Got {}".format(image.shape))

        r: torch.Tensor = image[..., 0, :, :]
        g: torch.Tensor = image[..., 1, :, :]
        b: torch.Tensor = image[..., 2, :, :]

        a1,b1,c1,d1,e1 = DataClass.color_conv_matrix(color_conv)

        #y: torch.Tensor = 0.299 * r + 0.587 * g + 0.114 * b
        #u: torch.Tensor = -0.147 * r - 0.289 * g + 0.436 * b
        #v: torch.Tensor = 0.615 * r - 0.515 * g - 0.100 * b
        y: torch.Tensor = a1 * r + b1 * g + c1 * b
        u: torch.Tensor = (b - y) / d1 + 0.5
        v: torch.Tensor = (r - y) / e1 + 0.5

        out: torch.Tensor = torch.stack([y, u, v], -3)

        return out        

    @staticmethod
    def extract_info(fn, default_bits = 10, default_fmt = '444'):
        import re
        wh = re.search("(?P<w>\d+)x(?P<h>\d+)", fn)
        b = re.search("(?P<b>\d+)bit", fn)

        w = wh.group('w')
        h = wh.group('h')

        b = default_bits if b is None else b.group('b')        
        fmt = default_fmt
        if "YUV444" in fn:
            fmt = "444"
        elif "YUV420" in fn:
            fmt = "420"
            raise NotImplementedError
            # TODO: add upsampling in YUV -> RGB convertion
        elif "sRGB" in fn:
            fmt = "sRGB"
        
        return int(w),int(h),int(b),fmt

    @staticmethod
    def read_yuv(filename, width, height, bits=8, out_plane_norm = [ 0, 1 ], fmt="444", device="cpu" ): 
        nr_bytes = int(np.ceil(bits / 8))
        if nr_bytes == 1:
            data_type = np.uint8
        elif nr_bytes == 2:
            data_type = np.uint16
        else:
            raise NotImplementedError('Reading more than 16-bits is currently not supported!')

        ans = {'Y': None, 'U': None, 'V': None}
        sizes = {'Y': [height, width], 'U': [height, width], 'V': [height, width]}

        if fmt == '420':
            for a in ['U', 'V']:
                sizes[a][0] >>= 1
                sizes[a][1] >>= 1
        elif fmt == '400':
            ans = {'Y': None}
            for a in ['U', 'V']:
                sizes[a][0] = 0
                sizes[a][1] = 0
        elif fmt == '444':
            pass
        else:
            raise NotImplementedError('The specified yuv format is not supported!')    

        for plane in ans:
            ans[plane] = torch.zeros(sizes[plane], dtype=torch.float, device=torch.device(device))
        
        with open(filename, "rb") as f:
            for plane in ['Y', 'U', 'V']:
                tmp = np.frombuffer( f.read(np.int(sizes[plane][0] * sizes[plane][1] * nr_bytes)), dtype=data_type)
                tmp = tmp.reshape(sizes[plane])

                ans[plane] = torch.tensor(( tmp.astype(np.float32) / (2**bits-1) ) * (max(out_plane_norm) - min(out_plane_norm)) + min(out_plane_norm), dtype=torch.float, device=torch.device(device))

        
        return ans


    def write_yuv(self, f, bits=None): 
        """
        dump a yuv file to the provided path
        @path: path to dump yuv to (file must exist)
        @bits: bitdepth
        @frame_idx: at which idx to write the frame (replace), -1 to append
        """
        if bits is None:
            bits = self.bitdepth
        yuv = self.yuv_data.copy()
        nr_bytes = np.ceil(bits / 8)
        if nr_bytes == 1:
            data_type = np.uint8
        elif nr_bytes == 2:
            data_type = np.uint16
        elif nr_bytes <= 4:
            data_type = np.uint32
        else:
            raise NotImplementedError('Writing more than 16-bits is currently not supported!')


        # rescale to range of bits
        for plane in yuv:
            yuv[plane] = DataClass.convertup_and_round_plane(yuv[plane], self.data_range, self.data_range, bits).cpu().numpy()

        # dump to file
        lst = []
        for plane in ['Y', 'U', 'V']:
            if plane in yuv.keys():
                lst = lst + yuv[plane].ravel().tolist()

        raw = np.array(lst)

        raw.astype(data_type).tofile(f)



class MetricParent:
    metric_name: ""

    def __init__(self, bits=10, max_val=1023, mvn = 1, name=""):
        self.__name = name
        self.bits = bits
        self.max_val = max_val
        self.__metric_val_number = mvn

    def set_bd_n_maxval(self, bitdepth = None, max_val = None):
        if bitdepth is not None:
            self.bits = bitdepth
        if max_val is not None:
            self.max_val = max_val

    def name(self):
        return self.__name

    def metric_val_number(self):
        return self.__metric_val_number

    def calc(self, orig, rec):
        raise NotImplementedError

class PSNRMetric(MetricParent):

    def __init__(self, *args, **kwards):
        super().__init__(*args, **kwards, mvn = 3, name=["PSNR_Y", "PSNR_U", "PSNR_V"])

    def calc(self, orig, rec):
        ans = []
        for plane in orig.yuv_data:
            a = orig.yuv_data[plane].mul((1 << self.bits)-1)
            b = rec.yuv_data[plane].mul((1 << self.bits)-1)
            mse = torch.mean((a - b) ** 2).item()
            if mse == 0.0:
                ans.append(100)
            else:
                ans.append(20 * np.log10(self.max_val) - 10 * np.log10(mse))
        return ans

class MSSSIMTorch(MetricParent):

    def __init__(self, *args, **kwards):
        super().__init__(*args, **kwards, name="MS-SSIM (PyTorch)")

    def calc(self, orig, rec):
        ans = 0.0
        from pytorch_msssim import ms_ssim
        if "Y" not in orig.yuv_data or "Y" not in rec.yuv_data:
            return -100.0
        plane = "Y"
        a = orig.yuv_data[plane].mul((1 << self.bits)-1)
        b = rec.yuv_data[plane].mul((1 << self.bits)-1)
        a.unsqueeze_(0).unsqueeze_(0)
        b.unsqueeze_(0).unsqueeze_(0)
        ans = ms_ssim(a, b, data_range=self.max_val).item()
        
        return ans

class MSSSIM_IQA(MetricParent):

    def __init__(self, *args, **kwards):
        super().__init__(*args, **kwards, name="MS-SSIM (IQA)")
        from IQA_pytorch.MS_SSIM import MS_SSIM
        self.ms_ssim = MS_SSIM(channels=1)

    def calc(self, orig, rec):
        ans = 0.0
        if "Y" not in orig.yuv_data or "Y" not in rec.yuv_data:
            return -100.0
        plane = "Y"
        b = orig.yuv_data[plane].unsqueeze(0).unsqueeze(0)
        a = rec.yuv_data[plane].unsqueeze(0).unsqueeze(0)
        ans = self.ms_ssim(a, b, as_loss=False).item()
        
        return ans        

class PSNR_HVS(MetricParent):

    def __init__(self, *args, **kwards):
        super().__init__(*args, **kwards, name="PSNR_HVS")

    def pad_img(self, img, mult):
        import torch.nn.functional as F
        import math
        h,w = img.shape[-2:]
        w_diff = int(math.ceil(w / mult) * mult) - w
        h_diff = int(math.ceil(h / mult) * mult) - h
        return F.pad(img, (0,w_diff, 0,h_diff), mode='replicate')
        
    def calc(self, orig, rec):
        from psnr_hvsm import psnr_hvs_hvsm

        a = orig.yuv_data['Y']
        b = rec.yuv_data['Y']
        a = DataClass.convert_range(a, orig.data_range, [0,1])
        b = DataClass.convert_range(b, rec.data_range, [0,1])
        a_img = self.pad_img(a.unsqueeze(0).unsqueeze(0), 8).squeeze()
        b_img = self.pad_img(b.unsqueeze(0).unsqueeze(0), 8).squeeze() 
        #a_img = a
        #b_img = b
        a_img = a_img.cpu().numpy().astype(np.float64)
        b_img = b_img.cpu().numpy().astype(np.float64)

        p_hvs, p_hvs_m = psnr_hvs_hvsm(a_img, b_img)
        
        return p_hvs  

class VIF_IQA(MetricParent):

    def __init__(self, *args, **kwards):
        super().__init__(*args, **kwards, name="VIF")
        from IQA_pytorch import VIFs
        self.vif = VIFs(channels=1)

    def calc(self, orig, rec):
        ans = 0.0
        if "Y" not in orig.yuv_data or "Y" not in rec.yuv_data:
            return -100.0
        plane = "Y"
        b = DataClass.convert_range(orig.yuv_data[plane].unsqueeze(0).unsqueeze(0), orig.data_range, [0,255])
        a = DataClass.convert_range(rec.yuv_data[plane].unsqueeze(0).unsqueeze(0), rec.data_range, [0,255])
        self.vif = self.vif.to(a.device)
        ans = self.vif(a, b, as_loss=False).item()
        
        return ans        

class FSIM_IQA(MetricParent):

    def __init__(self, *args, **kwards):
        super().__init__(*args, **kwards, name="FSIM")
        from IQA_pytorch import FSIM
        self.fsim = FSIM(channels=3)

    def calc(self, orig, rec):
        ans = 0.0
        
        b = orig.rgb_data
        a = rec.rgb_data
        self.fsim = self.fsim.to(a.device)
        ans = self.fsim(a, b, as_loss=False).item()
        
        return ans      

class NLPD_IQA(MetricParent):

    def __init__(self, *args, **kwards):
        super().__init__(*args, **kwards, name="NLPD")
        from IQA_pytorch import NLPD
        self.chan = 1
        self.nlpd = NLPD(channels=self.chan)

    def calc(self, orig, rec):
        ans = 0.0
        if "Y" not in orig.yuv_data or "Y" not in rec.yuv_data:
            return -100.0
        if self.chan == 1:
            plane = "Y"
            b = orig.yuv_data[plane].unsqueeze(0).unsqueeze(0)
            a = rec.yuv_data[plane].unsqueeze(0).unsqueeze(0)
        elif self.chan == 3:
            b = DataClass.convert_yuvdict_to_tensor(orig.yuv_data, orig.yuv_data['Y'].device)
            a = DataClass.convert_yuvdict_to_tensor(rec.yuv_data, rec.yuv_data['Y'].device)
        self.nlpd = self.nlpd.to(a.device)
        ans = self.nlpd(a, b, as_loss=False).item()
        
        return ans   

class IWSSIM(MetricParent):

    def __init__(self, *args, **kwards):
        super().__init__(*args, **kwards, name="IW-SSIM")
        from IW_SSIM_PyTorch import IW_SSIM
        self.iwssim = IW_SSIM()

    def calc(self, orig, rec):
        ans = 0.0
        if "Y" not in orig.yuv_data or "Y" not in rec.yuv_data:
            return -100.0
        plane = "Y"
        # IW-SSIM takes input in a range 0-255
        a = DataClass.convert_range(orig.yuv_data[plane], orig.data_range, [0,255])
        b = DataClass.convert_range(rec.yuv_data[plane], rec.data_range, [0,255])
        ans = self.iwssim.test(a.detach().cpu().numpy(), b.detach().cpu().numpy())
        
        return ans.item()

class VMAF(MetricParent):

    def __init__(self, *args, **kwards):
        super().__init__(*args, **kwards, name="VMAF")
        import platform
        if platform.system() == "Linux":
            self.URL="https://github.com/Netflix/vmaf/releases/download/v2.2.1/vmaf"
            self.OUTPUT_NAME=os.path.join(os.path.dirname(__file__), "vmaf.linux")
        else:
            # TODO: check that
            self.URL="https://github.com/Netflix/vmaf/releases/download/v2.2.1/vmaf.exe"
            self.OUTPUT_NAME=os.path.join(os.path.dirname(__file__), "vmaf.exe")

    def download(self, url, output_path):
        import requests
        r = requests.get(url, stream=True) #, verify=False)
        if r.status_code == 200:
            with open(output_path, 'wb') as f:
                for chunk in r:
                    f.write(chunk)

    def check(self):           
        if not os.path.exists(self.OUTPUT_NAME):
            import stat
            self.download(self.URL, self.OUTPUT_NAME)
            os.chmod(self.OUTPUT_NAME, stat.S_IEXEC)


    def calc(self, orig, rec):

        import tempfile
        import subprocess
        fp_o = tempfile.NamedTemporaryFile(delete=False)
        fp_r = tempfile.NamedTemporaryFile(delete=False)
        orig.write_yuv(fp_o, self.bits)
        rec.write_yuv(fp_r, self.bits)

        out_f =  tempfile.NamedTemporaryFile(delete=False)
        out_f.close()

        self.check()

        args = [
            self.OUTPUT_NAME,  "-r", fp_o.name, "-d", fp_r.name, "-w", str(orig.shape[1]), "-h", str(orig.shape[0]), "-p", "444", "-b", str(self.bits), "-o", out_f.name, "--json"
        ]
        subprocess.run(args,  stdout=subprocess.DEVNULL,  stderr=subprocess.DEVNULL)
        import json
        with open(out_f.name, "r") as f:
            tmp = json.load(f)
        ans = tmp['frames'][0]['metrics']['vmaf']
        
        os.unlink(fp_o.name)
        os.unlink(fp_r.name)
        os.unlink(out_f.name)

        return ans      


class WS(MetricParent):
    def __init__(self, *args, **kwards):
        super().__init__(*args, **kwards, name="WS")
 
    def calc(self, orig, orig_noisy,rec, orig_rec):
        import scipy.stats as stat 
        ans = 0.0
        gt = orig.rgb_data
        gt_noisy = orig_noisy.rgb_data
        rec_noisy = rec.rgb_data
        rec_orig = orig_rec.rgb_data

        gt       = DataClass.convert_range(gt, orig.data_range, [0,1])
        gt_noisy = DataClass.convert_range(gt_noisy, orig_noisy.data_range, [0,1])
        rec_noisy = DataClass.convert_range(rec_noisy, rec.data_range, [0,1])
        rec_orig = DataClass.convert_range(rec_orig, rec.data_range, [0,1])

        
        gt_img = gt.cpu().numpy().astype(np.float64).flatten()
        gt_noisy_img = gt_noisy.cpu().numpy().astype(np.float64).flatten()
        rec_noisy_img = rec_noisy.cpu().numpy().astype(np.float64).flatten()
        rec_orig_img = rec_orig.cpu().numpy().astype(np.float64).flatten()

        noise_gt = gt_noisy_img - gt_img
        noise_syn = rec_noisy_img - rec_orig_img

        ans = stat.wasserstein_distance(noise_gt,noise_syn)        
        return ans   

class KS(MetricParent):
    def __init__(self, *args, **kwards):
        super().__init__(*args, **kwards, name="KS")
 
    def calc(self, orig, orig_noisy,rec,orig_rec):
        import scipy.stats as stat 
        ans = 0.0
        gt = orig.rgb_data
        gt_noisy = orig_noisy.rgb_data
        rec_noisy = rec.rgb_data
        rec_orig = orig_rec.rgb_data

        gt       = DataClass.convert_range(gt, orig.data_range, [0,1])
        gt_noisy = DataClass.convert_range(gt_noisy, orig_noisy.data_range, [0,1])
        rec_noisy = DataClass.convert_range(rec_noisy, rec.data_range, [0,1])
        rec_orig = DataClass.convert_range(rec_orig, rec.data_range, [0,1])

        
        gt_img = gt.cpu().numpy().astype(np.float64).flatten()
        gt_noisy_img = gt_noisy.cpu().numpy().astype(np.float64).flatten()
        rec_noisy_img = rec_noisy.cpu().numpy().astype(np.float64).flatten()
        rec_orig_img = rec_orig.cpu().numpy().astype(np.float64).flatten()

        noise_gt = gt_noisy_img - gt_img
        noise_syn = rec_noisy_img - rec_orig_img

        ans = stat.ks_2samp((noise_gt).round(decimals=5),(noise_syn).round(decimals=5))[0]
        return ans

class KL(MetricParent):
    def __init__(self, *args, **kwards):
        super().__init__(*args, **kwards, name="KL")
 
    def calc(self, orig, orig_noisy,rec,orig_rec):
        from scipy.special import kl_div
        ans = 0.0
        gt = orig.rgb_data
        gt_noisy = orig_noisy.rgb_data
        rec_noisy = rec.rgb_data
        rec_orig = orig_rec.rgb_data

        gt       = DataClass.convert_range(gt, orig.data_range, [0,1])
        gt_noisy = DataClass.convert_range(gt_noisy, orig_noisy.data_range, [0,1])
        rec_noisy = DataClass.convert_range(rec_noisy, rec.data_range, [0,1])
        rec_orig = DataClass.convert_range(rec_orig, rec.data_range, [0,1])
        
        gt_img = gt.cpu().numpy().astype(np.float64).flatten()
        gt_noisy_img = gt_noisy.cpu().numpy().astype(np.float64).flatten()
        rec_noisy_img = rec_noisy.cpu().numpy().astype(np.float64).flatten()
        rec_orig_img = rec_orig.cpu().numpy().astype(np.float64).flatten()

        noise_gt = gt_noisy_img - gt_img
        noise_syn = rec_noisy_img - rec_orig_img
        #input("check noise range")
        bin_width=1
        bin_edges=np.linspace(-255.5/255.0,255.5/255.0,512)

        hist_noise_gt, bin_edges_noise_gt = np.histogram(noise_gt, bins=bin_edges)
        hist_noise_syn, bin_edges_noise_syn = np.histogram(noise_syn, bins=bin_edges)

        n_zero_bin_gt = np.sum(hist_noise_gt==0.)
        n_zero_bin_syn = np.sum(hist_noise_syn==0.)
        
        hist_noise_gt[hist_noise_gt==0.] = 1.0
        hist_noise_syn[hist_noise_syn==0.] = 1.0
         
        pmf_noise_gt = hist_noise_gt/(noise_gt.size + n_zero_bin_gt)
        pmf_noise_syn = hist_noise_syn/(noise_syn.size + n_zero_bin_syn)
        ans = np.sum(kl_div(pmf_noise_gt,pmf_noise_syn))
        return ans

class JS(MetricParent):
    def __init__(self, *args, **kwards):
        super().__init__(*args, **kwards, name="JS")
 
    def calc(self, orig, orig_noisy,rec,orig_rec):
        from scipy.spatial import distance
        ans = 0.0
        gt = orig.rgb_data
        gt_noisy = orig_noisy.rgb_data
        rec_noisy = rec.rgb_data
        rec_orig = orig_rec.rgb_data

        gt       = DataClass.convert_range(gt, orig.data_range, [0,1])
        gt_noisy = DataClass.convert_range(gt_noisy, orig_noisy.data_range, [0,1])
        rec_noisy = DataClass.convert_range(rec_noisy, rec.data_range, [0,1])
        rec_orig = DataClass.convert_range(rec_orig, rec.data_range, [0,1])
        
        gt_img = gt.cpu().numpy().astype(np.float64).flatten()
        gt_noisy_img = gt_noisy.cpu().numpy().astype(np.float64).flatten()
        rec_noisy_img = rec_noisy.cpu().numpy().astype(np.float64).flatten()
        rec_orig_img = rec_orig.cpu().numpy().astype(np.float64).flatten()

        noise_gt = gt_noisy_img - gt_img
        noise_syn = rec_noisy_img - rec_orig_img

        bin_width=1
        bin_edges=np.linspace(-255.5/255.0,255.5/255,512)

        hist_noise_gt, bin_edges_noise_gt = np.histogram(noise_gt, bins=bin_edges)
        hist_noise_syn, bin_edges_noise_syn = np.histogram(noise_syn, bins=bin_edges)

        n_zero_bin_gt = np.sum(hist_noise_gt==0.)
        n_zero_bin_syn = np.sum(hist_noise_syn==0.)
        
        hist_noise_gt[hist_noise_gt==0.] = 1.0
        hist_noise_syn[hist_noise_syn==0.] = 1.0
         
        pmf_noise_gt = hist_noise_gt/(noise_gt.size + n_zero_bin_gt)
        pmf_noise_syn = hist_noise_syn/(noise_syn.size + n_zero_bin_syn)
        ans = distance.jensenshannon(pmf_noise_gt,pmf_noise_syn)
        return ans     

class PSNR_noisy(MetricParent):
    def __init__(self, *args, **kwards):
        super().__init__(*args, **kwards, name="PSNR_noisy")
 
    def calc(self, orig, orig_noisy,rec,orig_rec):
        from scipy.spatial import distance
        ans = 0.0
        gt = orig.rgb_data
        gt_noisy = orig_noisy.rgb_data
        rec_noisy = rec.rgb_data
        
        gt       = DataClass.convert_range(gt, orig.data_range, [0,1])
        gt_noisy = DataClass.convert_range(gt_noisy, orig_noisy.data_range, [0,1])
        rec_noisy = DataClass.convert_range(rec_noisy, rec.data_range, [0,1])

        gt_img = gt.cpu().numpy().astype(np.float64).flatten()
        gt_noisy_img = gt_noisy.cpu().numpy().astype(np.float64).flatten()
        rec_noisy_img = rec_noisy.cpu().numpy().astype(np.float64).flatten()
        #PSNR calculation
        mse = np.mean((gt_noisy_img - rec_noisy_img) ** 2)
        if mse == 0:
            print("Warning: Two same images")
        ans =  10 * np.log10((1.0) / mse)
        return ans

class SSIM_noisy(MetricParent):
    def __init__(self, *args, **kwards):
        super().__init__(*args, **kwards, name="SSIM_noisy")
 
    def calc(self, orig, orig_noisy,rec,orig_rec):
        ans = 0.0
        from pytorch_msssim import SSIM,ssim
        gt = orig.rgb_data
        gt_noisy = orig_noisy.rgb_data
        rec_noisy = rec.rgb_data
        
        gt        = DataClass.convert_range(gt, orig.data_range, [0,1])
        gt_noisy  = DataClass.convert_range(gt_noisy, orig_noisy.data_range, [0,1])
        rec_noisy = DataClass.convert_range(rec_noisy, rec.data_range, [0,1])


        ans = ssim( gt_noisy, rec_noisy, data_range=1, size_average=False).item()
        return ans


class MetricsFabric:
    metrics_list_reconstruction = ["msssim_torch", "msssim_iqa", "psnr", "vif", "fsim", "nlpd", "iw-ssim", "vmaf", "psnr_hvs"]
    metrics_list_denoising = ["kl","js","ks","ws","psnr_noisy","ssim"] #,
    
    def __init__(self, bits={}, max_vals={}):
        self.bits = bits
        self.max_vals = max_vals
      
    @staticmethod  
    def get_metrics_list(task_type="all"):
        """
        Get list of metrics

        Args:
            task_type (str, optional): Type of task of requested metrics. 
                Possible values are: 
                    "reco" corresponds to reconstructed task,
                    "denoise" corresponds to denoising task,
                    "all" corresponds to all of above tasks.
                Defaults to "all".

        Returns:
            list: list with metrics' names
        """
        if task_type == "reco":
            return MetricsFabric.metrics_list_reconstruction
        elif task_type == "denoise":
            return MetricsFabric.metrics_list_denoising
        elif task_type == "all":
            return MetricsFabric.metrics_list_reconstruction + MetricsFabric.metrics_list_denoising

    def create_instance(self, name):
        name = name.lower()

        params = {}
        if isinstance(self.bits, dict):
            if name in self.bits:
                params["bits"] = self.bits[name]
            else:
                params["bits"] = 10
        else:
            params["bits"] = self.bits

        if isinstance(self.max_vals, dict):
            if name in self.max_vals:
                params["max_val"] = self.max_vals[name]
            else:
                params["max_val"] = (1 << params["bits"]) - 1
        else:
            params["max_val"] =  self.max_vals

        ans = None
        if name == "msssim_torch":
            ans = MSSSIMTorch(**params)
        elif name == "msssim_iqa":
            ans = MSSSIM_IQA(**params)
        elif name == "psnr":
            ans = PSNRMetric(**params)
        elif name == "vif":
            ans = VIF_IQA(**params)
        elif name == "fsim":
            ans = FSIM_IQA(**params)
        elif name == "nlpd":
            ans = NLPD_IQA(**params)
        elif name == "iw-ssim":
            ans = IWSSIM(**params)
        elif name == "vmaf":
            ans = VMAF(**params)
        elif name == "psnr_hvs":
            ans = PSNR_HVS(**params)
        elif name == "ws":
            return WS(**params)
        elif name == "ks":
            return KS(**params)
        elif name == "kl":
            return KL(**params)
        elif name == "js":
            return JS(**params)
        elif name == "psnr_noisy":
            return PSNR_noisy(**params)
        elif name =="ssim":
            return SSIM_noisy(**params)
        else:
            raise NotImplementedError
        
        ans.metric_name = name
        return ans


class MetricsProcessor:
    def __init__(self, task_type="reco"):
        self.internal_bits = -1
        self.jvet_psnr = False
        self.metrics = MetricsFabric.get_metrics_list(task_type)
        self.metrics_output = self.metrics
        self.color_conv = "709"
        self.metrics_fab = MetricsFabric()
        self.metrics_inst = {}
        self.f = None
        self.sep = '\t'
        self.is_csv = False
        for m in self.metrics:
            self.metrics_inst[m] = self.metrics_fab.create_instance(m) 

    def get_titles(self):
        titles = []
        titles.append("Reconstructed noisy")
        titles.append("Original noisy")
        titles.append("Reconstucted Original")
        titles.append("Original clean")
        titles.append("Codec")
        titles.append("BPP")
        for m_t in self.metrics_output:
            m = self.metrics_inst[m_t]
            name = m.name()
            if isinstance(name, list):
                titles += name
            else:
                titles.append(name)
        return titles


    def add_arguments(self, ap):
        ap.add_argument('--internal-bits', type=int, default=-1, choices=[-1,8,10], help='Bits for internal calculations (8,10 or determined based on internal data representation of the input file (default))')
        ap.add_argument("--jvet-psnr", default=False, action="store_true", help="Use 1020 as upper bound for 10 bits")
        ap.add_argument("--metrics", default=self.metrics, choices=self.metrics, nargs="+", help=f"Metrics to be used. Default: [{' '.join(self.metrics)}]")
        ap.add_argument("--metrics_output", default=self.metrics_output, choices=self.metrics_output, nargs="+", help=f"Order of metrics in output. Default: [{' '.join(self.metrics_output)}]")
        ap.add_argument('--color-conv', default="709", choices=["601", "709", "2020"], help="Color convertion notation")

    def parse_arguments(self, ap):
        args = ap.parse_args()
        self.internal_bits = args.internal_bits
        self.jvet_psnr = args.jvet_psnr
        self.metrics = args.metrics
        self.metrics_output = args.metrics_output
        self.color_conv = args.color_conv

    def __del__(self):
        self.close_file()

    @staticmethod
    def bpp_calc(filename, shape):
        bs_size = os.path.getsize(filename)
        bpp = bs_size * 8 / (shape[-2] * shape[-1])
        return bpp

    def process_images(self, orig: DataClass, rec: DataClass):
        bits = orig.bitdepth
        ans = []
        for m_t in self.metrics_output:
            if m_t in self.metrics:
                m = self.metrics_inst[m_t]
                if m.metric_name == "psnr" and self.jvet_psnr:
                    max_val = 256 * (1 << (bits-8))
                else:
                    max_val = (1 << bits) - 1
                m.set_bd_n_maxval(bits, max_val)

                tmp = m.calc(orig, rec)
                if isinstance(tmp, list):
                    ans += tmp
                else:
                    ans.append(tmp)
            else:
                ans.append(-100)
        return ans

    def process_noisy_images(self, orig: DataClass, orig_n:DataClass, rec: DataClass, orig_rec: DataClass):
        bits = orig.bitdepth
        ans = []
        for m_t in self.metrics_output:
            if m_t in self.metrics:
                m = self.metrics_inst[m_t]
                tmp = m.calc(orig, orig_n, rec, orig_rec)
                if isinstance(tmp, list):
                    ans += tmp
                else:
                    ans.append(tmp)
            else:
                ans.append(-100)
        return ans

    def process_image_files(self, orig_fn, rec_fn):
        data_o, target_bd = DataClass().load_image(orig_fn,def_bits=self.internal_bits, color_conv=self.color_conv)
        data_r, _ = DataClass().load_image(rec_fn,def_bits=target_bd, color_conv=self.color_conv)
        return self.process_images(data_o, data_r)

    def init_summary_file(self, fn, mode="txt"):
        self.f = open(fn, "w")
        self.sep = '\t'
        if mode == "csv":
            self.sep = ', '
            self.is_csv = True
            titles = self.get_titles()
            self.f.write(f"{self.sep.join(titles)}\n")

    def write_data(self, seq_name, bpp = None, metrics = None, prefix_data = None):
        if bpp is None:
            bpp = -1
        if metrics is None:
            metrics = []
            for m_t in self.metrics_output:
                if m_t in self.metrics_inst:
                    m = self.metrics_inst[m_t]
                    for _ in range(m.metric_val_number()):
                        metrics.append(-1)

        a = [bpp] + metrics
        a = [str(x) for x in a]
        if self.is_csv and prefix_data is not None:
            if isinstance(prefix_data, list):
                a = prefix_data + a
            else:
                a = [prefix_data] + a
        a = [seq_name] + a
        self.f.write(f"{self.sep.join(a)}\n")
        self.f.flush()

    def close_file(self):
        if self.f is not None:
            self.f.close()
        self.f = None
