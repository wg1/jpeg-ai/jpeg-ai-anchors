import argparse
import os
import re
from metrics import MetricsProcessor, DataClass

def find_files(lst, prefix, ext):
    ans = []
    for fn in lst:
        if fn.startswith(prefix) and fn.endswith(ext):
            ans.append(fn)
    return ans


def main():
    metrics = MetricsProcessor(task_type="denoise")
    ap = argparse.ArgumentParser()
    ap.add_argument('orig', default=os.getcwd(), help="Path original YUV/PNG file(s)")
    ap.add_argument('base_dir', default=None, help="Path to the base directory with results of different codecs")
    ap.add_argument('codec', default=None, help="Name of the codec")
    ap.add_argument('orig_noisy', default=os.getcwd(), help="Path original YUV/PNG file(s)")
    ap.add_argument('clean_rec', default=os.getcwd(), help="Path decoded originals file(s)")
    ap.add_argument('-b', default=None, help="Path to bitstreams")
    ap.add_argument('-lst', default=None, help="Path list of YUV/PNG file(s) to be processed")
    ap.add_argument('-r', default=None, help="Path reconstructed YUV/PNG file(s)")
    ap.add_argument('-s', default=None, help="Path output file with statistics")
    ap.add_argument('--format', default="png", choices=["yuv", "png"], help="Format of input image files")
    ap.add_argument('--bin-ext', default="bits", help="Extension of bin files")
    ap.add_argument('--no-bpp', default=False, action="store_true", help="Don't calculate bpp")
    ap.add_argument('-v', default=False, action="store_true", help="Verbose mode")
    ap.add_argument('--rates', nargs="+", type=int, default=[3,6,12,25,50,75,100,150,200], help="List of the rates to be checked")
    ap.add_argument('--n_noise', type=int, default=1, help="Which Relaization of noise ")
    ap.add_argument('--csv', default=False, action="store_true", help="Output file in CSV format")
    metrics.add_arguments(ap)

    args = ap.parse_args()
    metrics.parse_arguments(ap)

    if args.r is None:
        args.r = os.path.join(args.base_dir, args.codec, "rec")
    if args.b is None:
        args.b = os.path.join(args.base_dir, args.codec, "bit")
    if args.s is None:
        if args.csv:
            args.s = f"{args.codec}_summary.csv"
        else:
            args.s = f"{args.codec}_summary.txt"

    flst = []
    if args.lst is None or (not os.path.exists(args.lst)):
        flst = [x for x in os.listdir(args.orig) if os.path.splitext(x.lower())[1][1:] == args.format.lower()]
        flst = sorted(flst)
    else:
        with open(args.lst, "r") as iflst:
            for ifn in iflst:
                flst.append(ifn)

    lst_r = os.listdir(args.r)
    r = re.compile("(?P<name>.*)_(\d+)x(\d+)_(.+)_(?P<qp>\d+)")

    metrics.init_summary_file(args.s, "csv" if args.csv else "txt")

    for ifn in flst:
        # Iterate over all original files
        ifn = ifn.strip()
        bn, ext = os.path.splitext(ifn)
        #print("bn is", bn)
        bn_splits=bn.split("_")
        img_name_from_bn= bn_splits[0] + "_00"+str(args.n_noise)+"_"+bn_splits[1]+"_"+bn_splits[2]+"_"+bn_splits[3]+"_"+bn_splits[4]
        rec_fns = find_files(lst_r, f"{args.codec}_{img_name_from_bn}", ext)
        if args.v:
            print(f"Load original file {ifn}")

        #print("orig clean image is",ifn)
        data_o, target_bd = DataClass().load_image(os.path.join(args.orig,ifn),def_bits=metrics.internal_bits, color_conv=metrics.color_conv)
        orign_noisy_name = img_name_from_bn+ext
        #print("orig noisy image is",orign_noisy_name)
        data_o_noisy,_ = DataClass().load_image(os.path.join(args.orig_noisy,orign_noisy_name),def_bits=args.internal_bits, color_conv=args.color_conv)

        for rate in args.rates:
            #second approach
            orig_rec_name=f"{args.codec}_"+bn+f"_{rate:03d}{ext}"
            #print("orig rec name",orig_rec_name)
            data_o_rec, target_bd = DataClass().load_image(os.path.join(args.clean_rec,orig_rec_name),def_bits=metrics.internal_bits, color_conv=metrics.color_conv)

            # Iterate over rates
            rec_fn = find_files(rec_fns, "", f"{rate:03d}{ext}")
            
            rec_fn = rec_fn[0] if len(rec_fn) > 0 else ""

            bn_r, ext_r = os.path.splitext(rec_fn)
            n = r.search(bn_r)
            if n is not None:
                #bn_r = f"{n.group('name')}_n{args.n_noise}_{n.group('qp')}"
                #bn_r = bn_r
                bn_r = f"{args.codec}_"+bn_splits[0] + "_00"+str(args.n_noise)+"_"+bn_splits[1]+"_"+f"{rate:03d}"
            bs_fn = os.path.join(args.b,f"{bn_r}.{args.bin_ext}")
            #print("bs_fn is",bs_fn)
            if args.v:
                print(f"Start processing {rec_fn}. ", end="")
                
            if len(rec_fn) == 0 or not os.path.exists(os.path.join(args.r, rec_fn)):
                if args.v:
                    print(f"No reconstructed file with bitrate {rate} for original {ifn}")
                metrics.write_data(f"No_rec_for_{ifn}_RATE{rate:03d}")
            else:
                #print("noisy recon is",rec_fn)                
                data_r, _ = DataClass().load_image(os.path.join(args.r,rec_fn),def_bits=target_bd, color_conv=metrics.color_conv)
                #print(os.path.join(args.r,rec_fn))
                if args.no_bpp:
                    bpp = 0
                elif not os.path.exists(bs_fn):
                    if args.v:
                        print(f"No bitstream file {bs_fn}!")
                    bpp =-1
                else:
                    
                    bpp = metrics.bpp_calc(bs_fn, data_r.shape)
                #metrics_vals = metrics.process_images(data_o, data_r)
                metrics_vals = metrics.process_noisy_images(data_o, data_o_noisy,data_r,data_o_rec)
                #tmp = m.calc(data_o, data_o_noisy,data_r)
                if args.v:
                    print("Done.")
                prefix_data=[orign_noisy_name,bn,rec_fn, orig_rec_name, args.codec]
                metrics.write_data(rec_fn, bpp=bpp, metrics=metrics_vals, prefix_data=[orign_noisy_name,orig_rec_name, ifn, args.codec])
    metrics.close_file()


if __name__ == "__main__":
    main()