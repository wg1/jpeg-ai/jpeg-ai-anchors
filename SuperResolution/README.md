# JPEGAI SR Anchors

This repository contains the software for generating Super Resolution anchors for the JPEG AI Call for Proposal (CfP).

### Third party software

* Super Resolution (WDSR) https://github.com/krasserm/super-resolution (git submodule)
* Objective metrics https://gitlab.com/wg1/jpeg-ai/jpeg-ai-qaf (expected path ../metrics)

* FFMPEG Version 3.4.8


## Preparing data

All original test images must have dimentions that are multiple of 4.
Otherwise it will be padded.

**IMPORTANT:** Please download the test images from https://jpegai.github.io/test_images/
and put them in the `./data/original_images/` folder.


## Generating SR Anchors

1. Downsample the originals:
```
./downsample.sh
```

2. Upsample using classical methods:
```
./upsample.sh
```

3. Upsample using WDSR (DNN based method)
```
wget https://martin-krasser.de/sisr/weights-wdsr-b-32-x4.tar.gz
tar xvfz weights-wdsr-b-32-x4.tar.gz -C super-resolution/
cp process_anchors.py super-resolution/
cd super-resolution
conda env create -f environment.yml
conda activate sisr
python process_anchors.py
```

## Computing metrics

Setup the metrics:
1) cd ../metrics/
2) create conda environment: ``conda create -n jpeg_ai_metrics python=3.6.7``
3) activate environment: ``conda activate jpeg_ai_metrics``
4) Upgrade `pip` by a command `python -m pip install --upgrade pip`
5) install dependencies: ``pip install -r requirements.txt``
6) Download [vmaf.linux](https://github.com/Netflix/vmaf/releases/download/v2.1.1/vmaf.linux) version 2.1.1 to the root directory of the project by command:
``wget https://github.com/Netflix/vmaf/releases/download/v2.1.1/vmaf.linux``
7) make it executable: `chmod +x vmaf.linux`

Go back to SuperResolution directory:
```
cd ../SuperResolution
```

Run the following:
```
cp compute_jpegai_cfp_anchor_SR.sh ../metrics/
cd ../metrics/
bash compute_jpegai_cfp_anchor_SR.sh
cp *.csv ../SuperResolution/data/
```

## Related documents
1. ISO/IEC JTC1 SC29 WG1 Call_for_Proposals_for_JPEG_AI
2. ISO/IEC JTC1 SC29 WG1 JPEG_AI_Common_Training_and_Test_Conditions

