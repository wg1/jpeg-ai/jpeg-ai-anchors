import argparse
import os

class Base:
    def __init__(self, name, *args, **kwards):
        self.name = name
        self._sequential_execution = False      # Just for debuging
        self._dataset_path = os.path.join(os.getcwd(), "dataset")
        self._enc_dec_fn = Base.encode_decode
        self.output_dir = os.path.join(os.getcwd(), "Anchors")

    @staticmethod
    def encode_decode(cfg):
        """
        Default implementation function for encode/decode
        """
        pass

    def _set_enc_dec_func(self, func):
        """
        Setter for encode/decode function
        """
        self._enc_dec_fn = func

    def perform_metrics(self, metrics):
        """
        Placeholder for metrics execution for the current codec

        Args:
            metrics (metrics): class with metrics
        """
        pass
    
    def add_arguments(self, parser: argparse.ArgumentParser):
        parser.add_argument("--output", type=str, default=self.output_dir, help="Path to base output directory")
        parser.add_argument("--max-threads", type=int, default=-1, help="Number of maximum threads to be used. If it is -1, the number of cores will be used as the maximum number of threads.")
        parser.add_argument("-v", default=False, action="store_true", help="Verbose mode")
    
    def parse_arguments(self, parser: argparse.ArgumentParser):
        import multiprocessing
        p = parser.parse_args()
        self.output_dir = p.output
        self.verbose = p.v
        self.max_threads =  multiprocessing.cpu_count() if p.max_threads<=0 else p.max_threads
    
    def download_dataset(self):
        """
        Placeholder for downloading dataset
        """
        return False 
    
    @staticmethod
    def log_none(name, s):
        pass
    
    @staticmethod
    def log_str(name, s):
        print(f"{name}: {s}")
    
    def get_log_fn(self):
        return Base.log_str if self.verbose else Base.log_none
    
    def log(self, s):
        if self.verbose:
            self.__log_str(self.name, s)
    
    def download_weights(self):
        """
        Placeholder for downloading weights, code and other stuff
        """
        return False
    
    def _iter_over_dataset(self):
        """
        Iterate over files in dataset
        """
        yield ""
    
    def _generate_config(self, filename):
        """
        Generate configuration for encoder/decoder
        """
        pass
    
    def prerun_hook(self):
        os.makedirs(self.output_dir, exist_ok=True)
        
    @staticmethod
    def execute_with_check(cmd, exit_code, *args, **kwards):
        import subprocess
        ans = subprocess.run(cmd, *args, **kwards).returncode
        if ans != 0:
            print(f'Command "{" ".join(cmd)}" finished with code {ans}')
            exit(exit_code)

    def run(self):
        self.prerun_hook()
        if self._sequential_execution:
            # Iterate over all files one by one
            for fn in self._iter_over_dataset():
                cfg = self._generate_config(fn)
                if cfg is not None:
                    self._enc_dec_fn(cfg)
        else:
            import multiprocessing
            configurations = []
            # Generate configuration for all files
            for fn in self._iter_over_dataset():
                cfg = self._generate_config(fn)
                if cfg is not None:
                    configurations.append(cfg)
                
            # Execute encoder/decoder concurrently
            with multiprocessing.Pool(min(len(configurations), self.max_threads)) as p:
                ans=p.map(self._enc_dec_fn, configurations)
            #print(f"Ans: {ans}")
        
    @staticmethod
    def download(url, output_path):
        """
        Download file

        Args:
            url (str): URL of file
            output_path (str): destination path

        Returns:
            bool: success or not
        """
        import requests
        r = requests.get(url, stream=True, verify=False) #, verify=False)
        if r.status_code == 200:
            with open(output_path, 'wb') as f:
                for chunk in r:
                    f.write(chunk)
            return True
        return False
                
    @staticmethod
    def download_dir(url, output_path):
        """
        Download directory from URL. 
        Realized througt wget. Work only on Linux

        Args:
            url (str): URL to directory
            output_path (str): destination path

        Returns:
            int: error code. If it is 0, it downloaded successfuly
        """
        import subprocess
        import urllib.parse
        p_url = urllib.parse.urlparse(url)
        levels = len([x for x in p_url.path.split("/") if len(x)>0])
        return subprocess.call(['wget', '-e', 'robots=off', '--recursive', '--no-parent', "-nH", url, "-P", output_path, "--cut-dirs", str(levels)])


def make_main_func(obj):
    parser = argparse.ArgumentParser(conflict_handler='resolve')
    parser.add_argument("--only-metrics", default=False, action="store_true", help="Perform only metrics calculation")

    objs = [obj] if isinstance(obj, Base) else obj
    for o in objs:   
        o.add_arguments(parser)
    for o in objs:
        o.parse_arguments(parser)
        args = parser.parse_args()
        if o.download_dataset():
            if o.download_weights():
                if not args.only_metrics:
                    o.run()
                o.perform_metrics()
            else:
                print(f"Cannot download weights/code for {o.name}")
        else:
            print(f"Cannot download dataset by {o.name}")
