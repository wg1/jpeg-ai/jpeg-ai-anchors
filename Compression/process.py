from common.Base import Base
import argparse
import os
import subprocess
import re

class CodecParent(Base):
    
    def __init__(self, name, input_ext, *args, **kwards):
        super(CodecParent, self).__init__(name, *args, *kwards)
        self._cur_qp = 0
        self._tar_cbpp = 0
        self.input_ext = input_ext
        self.val_conv_path = os.path.join(os.getcwd(), f"dataset_{input_ext}")
        self.rec_path = os.path.join(os.path.abspath(os.path.dirname(__file__)), "rec")
        self.bit_path = os.path.join(os.path.abspath(os.path.dirname(__file__)), "bit")        
        if input_ext == "yuv":
            self._dataset_conv_func = self._convert_dataset_to_yuv
        elif input_ext == "ppm":
            self._dataset_conv_func = self._convert_dataset_to_ppm
        elif input_ext == "pnm":
            self._dataset_conv_func = self._convert_dataset_to_pnm
        else:
            self.val_conv_path = self._dataset_path
            self._dataset_conv_func = None
            
    def _default_list_file(self):
        return os.path.join(os.path.abspath(os.path.dirname(__file__)), self.name, "qps.txt")
    
    def download_weights(self):
        return self.download_code() and self.compile_code()
    
    def download_code(self):
        return False
    
    def compile_code(self):
        return False
    
    @staticmethod
    def _call_with_time_measurment(*args, **kwards):
        from datetime import datetime
        import subprocess
        start_time = datetime.now()
        ans = subprocess.call(*args, **kwards)
        total_time = datetime.now() - start_time
        return ans, total_time.total_seconds()        

    def perform_metrics(self):
        import sys
        output_fn = os.path.join(self.output_dir, f"{self.name}_summary.txt")
        cmd = [sys.executable, "main.py", self._dataset_path, self.output_dir, self.name, "-s",  output_fn, "-v"]
               #f_tmp.name, "-v"]
        subprocess.call(cmd, cwd=os.path.join(os.getcwd(), "metrics"))

        #output_fn = os.path.join(self.output_dir, f"{self.name}_summary.txt")
        #param_ext = re.compile(f"{self.name}_(?P<name>.*)_(?P<w>\d+)x(?P<h>\d+)_(.+)_(?P<bpp>\d+)")
#
        #with open(f_tmp.name, "r") as f_in, open(output_fn, "w") as f_out:
        #    for l in f_in:
        #        l_arr = [x.strip() for x in l.split("\t")]
        #        rec_fn = l_arr[0].strip()
        #        p = param_ext.search(rec_fn)
        #        if p is None:
        #            # kMAC/pxl, DecT_GPU, DecT_CPU, EncT_GPU, EncT_CPU
        #            l_arr_out = l_arr + ["-1", "0", "0", "0", "0"]
        #        else:
        #            log_fn = self._get_log_fn(p.group('name'), p.group('w'), p.group('h'), p.group('bpp'))
        #            enc_t, dec_t = self._load_time_from_log(os.path.join(self.rec_path, log_fn))
        #            # kMAC/pxl, DecT_GPU, DecT_CPU, EncT_GPU, EncT_CPU
        #            l_arr_out = l_arr + ["-1", str(dec_t), str(dec_t), str(enc_t), str(enc_t)]
        #        f_out.write("\t".join(l_arr_out) + "\n")
        
        
        
    def add_arguments(self, parser: argparse.ArgumentParser):
        super().add_arguments(parser)
        parser.add_argument(f"--{self.name}_list", type=str, default=self._default_list_file(), help="File with list of preset QPs")
        parser.add_argument(f"--{self.name}_qp_start", type=int, default=0, help="Start QP for iteration (if value of `list` is empty)")
        parser.add_argument(f"--{self.name}_qp_end", type=int, default=0, help="End QP for iteration (if value of `list` is empty)")
        parser.add_argument(f"--{self.name}_files_mask", type=str, default=".*", help="RegEx mask for name of files to be processed")
        parser.add_argument(f"--{self.name}_qp_search", default=False, action="store_true", help="Searching QP")
    
    def parse_arguments(self, parser: argparse.ArgumentParser):
        super().parse_arguments(parser)
        args = vars(parser.parse_args())
        self.lst_path = args[f"{self.name}_list"]
        self.qp_start = args[f"{self.name}_qp_start"]
        self.qp_end = args[f"{self.name}_qp_end"]
        self.rec_path = os.path.join(self.output_dir, self.name, "rec")
        self.bit_path = os.path.join(self.output_dir, self.name, "bit")
        self.file_name_r = re.compile(args[f"{self.name}_files_mask"])
        self.qp_search = args[f"{self.name}_qp_search"]
    
    def _iter_over_all_qps(self):
        for fn in os.listdir(self.val_conv_path):
            if self.file_name_r.match(fn):
                for qp in range(self.qp_start, self.qp_end+1):
                    self._cur_qp = qp
                    self._tar_cbpp = -1
                    yield fn
    
    def _iter_over_list(self):
        with open(self.lst_path, "r") as f:
            """
            Structure of the file:
            <Input image>\t<Target BPP>\t<QP>
            """
            last_fn = ""
            for l in f:
                ll = l.split("\t")
                self._tar_cbpp = int(ll[1].strip())
                if self.qp_search:
                    if last_fn != ll[0]:
                        qp_list = []
                        last_fn = ll[0]
                    cur_qp = int(ll[2].strip())
                    for qp in range(self.qp_start+cur_qp, cur_qp+self.qp_end+1):
                        self._cur_qp = qp
                        if qp not in qp_list and self.file_name_r.match(ll[0].strip()):
                            qp_list.append(qp)
                            yield ll[0].strip() + f".{self.input_ext}"                    
                else:
                    self._cur_qp = int(ll[2].strip())
                    if self.file_name_r.match(ll[0].strip()):
                        yield ll[0].strip() + f".{self.input_ext}"
    
    def _iter_over_dataset(self):
        if len(self.lst_path) == 0:
            f = self._iter_over_all_qps
        else:
            f = self._iter_over_list
        for n in f():
            if n.endswith(f".{self.input_ext}"):
                yield n
            
    def _set_dataset_conv_func(self, fn):
        self._dataset_conv_func = fn
        
    @staticmethod
    def _convert_yuv_to_png(in_fn, out_fn, width, height):        
        # Convert to PNG
        cmd = ["ffmpeg","-f", "rawvideo", 
            "-vcodec", "rawvideo", 
            "-s", f"{width}x{height}",
            "-r", "25",
            "-pix_fmt", "yuv444p10le",
            "-i", in_fn,
            "-pix_fmt", "rgb24",
            "-vf", "scale=in_range=full:in_color_matrix=bt709:out_range=full:out_color_matrix=bt709",
            "-color_primaries", "bt709", 
            "-color_trc", "bt709", 
            "-colorspace", "bt709",
            "-y", out_fn]
        ans, t = CodecParent._call_with_time_measurment(cmd)
        if ans != 0:
            print(f"Cannot convert YUV to PNG: {in_fn} -> {out_fn}")
            exit(-13)
        return ans, t

    @staticmethod
    def _convert_formats(in_fn, out_fn):        
        # Convert to PNG
        cmd = ["convert", in_fn, out_fn]
        ans, t = CodecParent._call_with_time_measurment(cmd, timeout=60)
        if ans != 0:
            print(f"Cannot convert: {in_fn} -> {out_fn}")
            exit(-13)
        return ans, t
                        
    def _convert_dataset_to_yuv(self):
        # Convert dataset to YUV
        if not os.path.exists(self.val_conv_path):
            os.makedirs(self.val_conv_path)
            for f in os.listdir(self._dataset_path):
                if f.endswith(".png"):
                    fn,_ = os.path.splitext(f)
                    output_file_name = f"{fn}.yuv"
                    print(f"Convert file {f} to {output_file_name}.", end="")
                    cmds=["ffmpeg", "-hide_banner", \
                            "-i", os.path.join(self._dataset_path, f), \
                            "-pix_fmt", "yuv444p10le", \
                            "-vf", "scale=in_range=full:in_color_matrix=bt709:out_range=full:out_color_matrix=bt709", \
                            "-color_primaries", "bt709", "-color_trc", "bt709", "-colorspace", "bt709", \
                            "-y", os.path.join(self.val_conv_path, output_file_name)]
                    #print(" ".join(cmds))
                    ans = subprocess.call(cmds)
                    print("Done" if ans == 0 else f"Error {ans}")
                    
    def _convert_dataset_to_ppm(self):
        # Convert dataset to PPM
        if not os.path.exists(self.val_conv_path):
            os.makedirs(self.val_conv_path)
            for f in os.listdir(self._dataset_path):
                if f.endswith(".png"):
                    fn,_ = os.path.splitext(f)
                    output_file_name = f"{fn}.ppm"
                    print(f"Convert file {f} to {output_file_name}.", end="")
                    cmds=["convert", os.path.join(self._dataset_path, f), "-strip", os.path.join(self.val_conv_path, output_file_name)]
                    #print(" ".join(cmds))
                    ans = subprocess.call(cmds) 
                    print("Done" if ans == 0 else f"Error {ans}")           
                    
    def _convert_dataset_to_pnm(self):
        # Convert dataset to PNM
        # Format is the same as PPM, but some codec supports PNM as input and doesn't support PPM
        if not os.path.exists(self.val_conv_path):
            os.makedirs(self.val_conv_path)
            for f in os.listdir(self._dataset_path):
                if f.endswith(".png"):
                    fn,_ = os.path.splitext(f)
                    output_file_name = f"{fn}.pnm"
                    print(f"Convert file {f} to {output_file_name}.", end="")
                    cmds=["convert", os.path.join(self._dataset_path, f), "-strip", os.path.join(self.val_conv_path, output_file_name)]
                    #print(" ".join(cmds))
                    ans = subprocess.call(cmds) 
                    print("Done" if ans == 0 else f"Error {ans}")                                  
            
    def download_dataset(self):
        # Download dataset
        ans = True
        if not os.path.exists(self._dataset_path):
            # TODO: add downloader 
            print(f"Please download dataset manualy to folder {self._dataset_path}")
            exit(-100)
        
        if ans and self._dataset_conv_func is not None:
            self._dataset_conv_func()
                    
        return ans
        

                
    def _generate_config(self, filename):
        cfg = {}
        qp = self._cur_qp
        fn = os.path.splitext(filename)[0]
        s = self._r.search(filename)
        gen_rec = True
        if self._tar_cbpp == -1 or self.qp_search:
            gen_rec = False
            rec_fn = os.path.join(self.rec_path, f"{qp}_{fn}.{self.input_ext}")
            bit_fn = os.path.join(self.bit_path, f"{qp}_{fn}.bits")
            png_fn = os.path.join(self.rec_path, f"{qp}_{fn}.png")
        else:
            bpp = f"{self._tar_cbpp:#03d}"
            fn_short = s.group('name')
            # Correct that place
            png_fn = os.path.join(self.rec_path, f"{self.name}_{fn}_8bit_sRGB_{bpp}.png")
            rec_fn = os.path.join(self.rec_path, f"{self.name}_{fn}_{bpp}.{self.input_ext}")
            bit_fn = os.path.join(self.bit_path, f"{self.name}_{fn_short}_{bpp}.bits")
            
        cfg['enc_path'] = self.enc_path
        cfg['dec_path'] = self.dec_path
        cfg['cfg_path'] = self.cfg_path
        cfg['width'] = s.group("width")
        cfg['height'] = s.group("height")
        cfg['qp'] = qp
        cfg['f'] = filename
        cfg['val_conv_path'] = self.val_conv_path
        cfg['bit_fn'] = bit_fn
        cfg['rec_fn'] = rec_fn
        cfg['png_fn'] = png_fn
        cfg['gen_rec'] = gen_rec
        cfg['codec_name'] = self.name
        cfg['log_fn'] = self.get_log_fn()
        if os.path.exists(bit_fn) and os.path.getsize(bit_fn) > 0:
            return None
        return cfg

    def prerun_hook(self):        
        os.makedirs(self.rec_path, exist_ok=True)
        os.makedirs(self.bit_path, exist_ok=True)
                

    
if __name__ == "__main__":
    from common.Base import make_main_func
    from . import get_models
    
    make_main_func(get_models())