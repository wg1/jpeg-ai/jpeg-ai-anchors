from ..process import CodecParent
import os
import subprocess
import re
import argparse

class JPEGXLCodec(CodecParent):
    def __init__(self, *args, **kwards):
        super(JPEGXLCodec, self).__init__("JPEGXL", "png", *args, **kwards)
        self.src_path = os.path.join(os.path.abspath(os.path.dirname(__file__)), "src")
        self.jpegxl_path = os.path.join(self.src_path, "libjxl")
        self.enc_path = os.path.join(self.jpegxl_path, "build", "tools", "cjxl")
        self.dec_path = os.path.join(self.jpegxl_path, "build", "tools", "djxl")
        self.cfg_path = self.jpegxl_path
        self._resampling = 1
        self._r = re.compile("(?P<name>.+)_(?P<width>\d+)x(?P<height>\d+)")
        self._set_enc_dec_func(self.jpegxl_enc_dec)
        
    @staticmethod
    def jpegxl_enc_dec(cfg):
        import subprocess
        enc_path = cfg['enc_path']
        dec_path = cfg['dec_path']
        cfg_path = cfg['cfg_path']
        width = cfg['width']
        height = cfg['height']
        qp = cfg['qp']
        f = cfg['f']
        val_conv_path = cfg['val_conv_path']
        bit_fn = cfg['bit_fn']
        rec_fn = cfg['rec_fn']
        png_fn = cfg['png_fn']
        gen_rec = cfg['gen_rec']
        resampling = cfg['resampling']
        enc_t, dec_t = -1, -1

        # Encode
        cmd = [enc_path, "-d", str(qp), "--strip", f"--resampling={resampling}", 
                                        os.path.join(val_conv_path, f),
                                        bit_fn]
        ans, enc_t = CodecParent._call_with_time_measurment(cmd)
        if ans != 0:
            print(f"Cannot encode {f}")
            exit(-12)
            
        if gen_rec:
            # Decode
            cmd = [dec_path, bit_fn, png_fn]
            ans, dec_t = CodecParent._call_with_time_measurment(cmd)
            if ans != 0:
                print(f"Cannot decode {bit_fn}")
                exit(-13)
                            
        from metrics.metrics import MetricsProcessor
        MetricsProcessor.store_complexity_info(png_fn, encCPU=enc_t, encGPU=enc_t, decCPU=dec_t, decGPU=dec_t)
        

    def download_code(self):
        ans = True
        # Download source ocde of VTM
        if not os.path.exists(self.jpegxl_path):
            os.makedirs(self.src_path)
            self.execute_with_check(["git", "clone", "https://github.com/libjxl/libjxl.git", "--recursive", "--shallow-submodules"], -120, cwd=self.src_path)
            self.execute_with_check(["git", "checkout", "-b", "tmp", "7d0fd6bdfc602afd7d7050f9aa4be58d2a92fc0f"], -121, cwd=self.jpegxl_path)
            self.execute_with_check(["git", "submodule", "update", "--init", "--recursive", "--depth", "1", "--recommend-shallow"], -122, cwd=self.jpegxl_path)
            self.execute_with_check(["git", "submodule", "foreach", "git", "fetch", "--unshallow"], -123, cwd=self.jpegxl_path)
            self.execute_with_check(["git", "submodule", "update", "--init", "--recursive"], -124, cwd=self.jpegxl_path)
        
        return ans                

    def compile_code(self):
        if not os.path.exists(self.enc_path) or not os.path.exists(self.dec_path):
            build_path = os.path.join(self.jpegxl_path, "build") 
            os.makedirs(build_path, exist_ok=True)
            #e = {"CC": "clang", "CXX": "clang++"}
            self.execute_with_check(["sudo", "apt", "install", "cmake", "pkg-config", "libbrotli-dev", "libgflags-dev", "libpng-dev", "clang"], -1000)
            self.execute_with_check(["cmake", "-DCMAKE_BUILD_TYPE=Release", "-DBUILD_TESTING=OFF", ".."], -100, cwd=build_path)
            self.execute_with_check(["cmake", "--build", ".", "--", "-j2"], -100, cwd=build_path) #, env=e)
            #ans = os.system(f"cd {self.jpeg_path} && make")
        return True
        
    def _generate_config(self, filename):
        ans = super()._generate_config(filename)
        ans['resampling'] = self._resampling
        return ans
        
    def _iter_over_dataset(self):
        assert len(self.lst_path) > 0
        with open(self.lst_path, "r") as f:
            """
            Structure of the file:
            <Input image>\t<Target BPP>\t<QP>
            """
            for l in f:
                ll = l.split("\t")
                self._cur_qp = float(ll[2].strip())
                self._tar_cbpp = int(ll[1].strip())
                self._resampling = int(ll[3].strip())
                if self.file_name_r.match(ll[0].strip()):
                    yield ll[0].strip() + f".{self.input_ext}"
    
if __name__ == "__main__":
    from common.Base import make_main_func
    obj = JPEGXLCodec()
    make_main_func(obj)