from ..process import CodecParent
import os
import subprocess
import re
import argparse

class VVCCodec(CodecParent):
    def __init__(self, *args, **kwards):
        super(VVCCodec, self).__init__("VVC", "yuv", *args, **kwards)
        self.src_path = os.path.join(os.path.abspath(os.path.dirname(__file__)), "src")
        self.vvc_path = os.path.join(self.src_path, "VVCSoftware_VTM-VTM-11.1")
        self.enc_path = os.path.join(self.vvc_path, "bin", "EncoderAppStatic")
        self.dec_path = os.path.join(self.vvc_path, "bin", "DecoderAppStatic")
        self.cfg_path = os.path.join(self.vvc_path, "cfg", "encoder_intra_vtm.cfg")
        self.scc_lst_path = os.path.join(os.path.abspath(os.path.dirname(__file__)), "scc.lst")
        self.scc_cfg_path = os.path.join(self.vvc_path, "cfg", "per-class", "classSCC.cfg")
        self._r = re.compile("(?P<name>.+)_(?P<width>\d+)x(?P<height>\d+)")
        self._set_enc_dec_func(self.vvc_enc_dec)
        
    @staticmethod
    def vvc_enc_dec(cfg):
        import subprocess
        enc_path = cfg['enc_path']
        dec_path = cfg['dec_path']
        cfg_path = cfg['cfg_path']
        width = cfg['width']
        height = cfg['height']
        qp = cfg['qp']
        f = cfg['f']
        val_conv_path = cfg['val_conv_path']
        bit_fn = cfg['bit_fn']
        rec_fn = cfg['rec_fn']
        png_fn = cfg['png_fn']
        gen_rec = cfg['gen_rec']
        enc_t, dec_t = -1, -1

        # Encode
        cmd = [enc_path]
        if isinstance(cfg_path, list):
            for cfg in cfg_path:
                cmd.append("-c")
                cmd.append(cfg)
        else:
            cmd.append("-c")
            cmd.append(cfg_path)

        cmd += ["-i", os.path.join(val_conv_path, f),
                                        "-wdt", width,
                                        "-hgt", height,
                                        "-b", bit_fn,
                                        "-f", "1",
                                        "-fr", "10",
                                        "-q", str(qp),
                                        "--InputBitDepth=10",
                                        "--InputChromaFormat=444",
                                        "--ChromaFormatIDC=444",
                                        "--TemporalSubsampleRatio=1",
                                        "--ConformanceWindowMode=1",
                                        "--Level=6.2"
                                        ]
        ans, enc_t = CodecParent._call_with_time_measurment(cmd)
        if ans != 0:
            print(f"Cannot encode {f}")
            exit(-12)
            
        if gen_rec:
            # Decode
            cmd = [dec_path, "-d", "10", 
                                                "-b", bit_fn,
                                                "-o", rec_fn]
            ans, dec_t = CodecParent._call_with_time_measurment(cmd)
            if ans != 0:
                print(f"Cannot decode {bit_fn}")
                exit(-13)
                
            # Convert to PNG
            _, conv_t = CodecParent._convert_yuv_to_png(rec_fn, png_fn, width, height)
            
            from metrics.metrics import MetricsProcessor
            MetricsProcessor.store_complexity_info(png_fn, encCPU=enc_t, encGPU=enc_t, decCPU=dec_t+conv_t, decGPU=dec_t+conv_t)


    def download_code(self):
        ans = True
        # Download source ocde of VTM
        if not os.path.exists(self.vvc_path):
            os.makedirs(self.src_path, exist_ok=True)
            tar_path = os.path.join(self.src_path, "src.tar")
            # see document wg1m93056 (https://sd.iso.org/documents/ui/#!/browse/iso/iso-iec-jtc-1/iso-iec-jtc-1-sc-29/iso-iec-jtc-1-sc-29-wg-1/library/6/93-Online/INPUT%20M-documents/wg1m93056-ICQ-Proposed%20Revision%20of%20JPEG%20AI%20Common%20Training%20and%20Testing%20Conditions)
            ans_tmp = True
            if not os.path.exists(tar_path):
                ans_tmp = self.download("https://vcgit.hhi.fraunhofer.de/jvet/VVCSoftware_VTM/-/archive/VTM-11.1/VVCSoftware_VTM-VTM-11.1.tar", tar_path)
            if ans_tmp:
                import tarfile
                with tarfile.open(tar_path, "r") as tar:
                    tar.extractall(self.src_path)                
            ans = ans_tmp 
                    
        return ans                

    def compile_code(self):
        if not os.path.exists(self.enc_path) or not os.path.exists(self.dec_path):
            ans = subprocess.call(["cmake", "."], cwd=os.path.join(self.vvc_path)) == 0 and \
                  subprocess.call(["make"], cwd=os.path.join(self.vvc_path)) == 0
            return ans 
        else:
            return True

    def _generate_config(self, filename):
        cfg = super()._generate_config(filename)
        if cfg is None:
            return None
        is_SCC = False
        with open(self.scc_lst_path, "r") as f:
            fn = os.path.splitext(filename)[0]
            for l in f:
                l_fn = os.path.splitext(l)[0]
                if l_fn.strip().lower() == fn.lower():
                    is_SCC = True
        if is_SCC:
            cfg["cfg_path"] = [cfg["cfg_path"], self.scc_cfg_path]

        return cfg


    
if __name__ == "__main__":
    from common.Base import make_main_func
    obj = VVCCodec()
    make_main_func(obj)