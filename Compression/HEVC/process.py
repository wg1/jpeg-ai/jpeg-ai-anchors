from ..process import CodecParent
import os
import subprocess
import re
import argparse

class HEVCCodec(CodecParent):
    def __init__(self, *args, **kwards):
        super(HEVCCodec, self).__init__("HEVC", "yuv", *args, **kwards)
        self.hevc_path = os.path.join(os.path.abspath(os.path.dirname(__file__)), "src")
        self.enc_path = os.path.join(self.hevc_path, "bin", "TAppEncoderStatic")
        self.dec_path = os.path.join(self.hevc_path, "bin", "TAppDecoderStatic")
        self.cfg_path = os.path.join(os.path.abspath(os.path.dirname(__file__)), "encoder_intra_main_scc_10.cfg")
        self._r = re.compile("(?P<name>.+)_(?P<width>\d+)x(?P<height>\d+)")
        self._set_enc_dec_func(self.hevc_enc_dec)

    @staticmethod
    def hevc_enc_dec(cfg):
        import subprocess
        enc_path = cfg['enc_path']
        dec_path = cfg['dec_path']
        cfg_path = cfg['cfg_path']
        width = cfg['width']
        height = cfg['height']
        qp = cfg['qp']
        f = cfg['f']
        val_conv_path = cfg['val_conv_path']
        bit_fn = cfg['bit_fn']
        rec_fn = cfg['rec_fn']
        png_fn = cfg['png_fn']
        gen_rec = cfg['gen_rec']
        enc_t, dec_t = -1, -1

        # Encode
        cmd = [enc_path,"-c", cfg_path,
                                        "-i", os.path.join(val_conv_path, f),
                                        "-wdt", width,
                                        "-hgt", height,
                                        "-b", bit_fn,
                                        "-f", "1",
                                        "-fr", "25",
                                        "-q", str(qp),
                                        "--FrameSkip=0",
                                        "--InputBitDepth=10",
                                        "--InputChromaFormat=444",
                                        "--ChromaFormatIDC=444",
                                        "--ConformanceWindowMode=1",
                                        "--Level=6.2"
                                        ]
        ans, enc_t = CodecParent._call_with_time_measurment(cmd)
        
        if ans != 0:
            print(f"Cannot encode {f}")
            exit(-12)

        if gen_rec:
            # Decode
            cmd = [dec_path, "-d", "10",
                                                "-b", bit_fn,
                                                "-o", rec_fn]
            ans, dec_t = CodecParent._call_with_time_measurment(cmd)
            
            if ans != 0:
                print(f"Cannot decode {bit_fn}")
                exit(-13)

            # Convert to PNG
            _, conv_t = CodecParent._convert_yuv_to_png(rec_fn, png_fn, width, height)
            
            from metrics.metrics import MetricsProcessor
            MetricsProcessor.store_complexity_info(png_fn, encCPU=enc_t, encGPU=enc_t, decCPU=dec_t+conv_t, decGPU=dec_t+conv_t)


    def download_code(self):
        ans = True
        # Download source ocde of HM
        if not os.path.exists(self.hevc_path):
            # see document wg1m93056 (https://sd.iso.org/documents/ui/#!/browse/iso/iso-iec-jtc-1/iso-iec-jtc-1-sc-29/iso-iec-jtc-1-sc-29-wg-1/library/6/93-Online/INPUT%20M-documents/wg1m93056-ICQ-Proposed%20Revision%20of%20JPEG%20AI%20Common%20Training%20and%20Testing%20Conditions)
            ans_tmp = self.download_dir("https://hevc.hhi.fraunhofer.de/svn/svn_HEVCSoftware/tags/HM-16.20+SCM-8.8/", self.hevc_path)
            ans = ans_tmp == 0

        return ans

    def compile_code(self):
        if not os.path.exists(self.enc_path) or not os.path.exists(self.dec_path):
            ans = subprocess.call(["make"], cwd=os.path.join(self.hevc_path, "build", "linux"))
            return ans == 0
        else:
            return True

if __name__ == "__main__":
    from common.Base import make_main_func
    obj = HEVCCodec()
    make_main_func(obj)