#!/bin/bash

INPUT_REC_DIR=$1
OUTPUT_SUMMARY_DIR=`realpath $2`

CUR_DIR=`pwd`
for qp in 12 25 50 75
do
	cd ${CUR_DIR}
	./decoded_anchor.sh ${INPUT_REC_DIR} ${qp} ${OUTPUT_SUMMARY_DIR}_${qp}
	
done