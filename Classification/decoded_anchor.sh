#!/bin/bash

INPUT_REC_DIR=$1
QP=$2
OUTPUT_SUMMARY_DIR=$3

cd ..
python -m Classification.process --Classification.data_dir ${INPUT_REC_DIR} --Classification.files_mask .*0${QP}\.png --output ${OUTPUT_SUMMARY_DIR}